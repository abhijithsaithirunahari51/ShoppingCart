package com.abhijithsai.shoppingcart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CartLineInfo {
	private ProductInfo productInfo;
	private int quantity;
	
	public double getAmount() {
	return this.productInfo.getPrice()*this.quantity;
	}
}
