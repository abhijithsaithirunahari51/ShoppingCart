package com.abhijithsai.shoppingcart.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDetailInfo {
	private String productCode;
	private String productName;
	
	private int quantity;
	private double price;
	private double amount;
	
}
