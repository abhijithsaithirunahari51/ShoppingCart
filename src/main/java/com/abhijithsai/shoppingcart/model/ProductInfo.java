package com.abhijithsai.shoppingcart.model;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.abhijithsai.shoppingcart.entity.Product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductInfo {
	private String code;
	private String name;
	private double price;

	private boolean newProduct = false;

	private CommonsMultipartFile fileData;

	public ProductInfo(Product product) {
		this.code = product.getCode();
		this.name = product.getName();
		this.price = product.getPrice();
	}

	public ProductInfo(String code, String name, double price) {
		this.code = code;
		this.name = name;
		this.price = price;
	}
}
