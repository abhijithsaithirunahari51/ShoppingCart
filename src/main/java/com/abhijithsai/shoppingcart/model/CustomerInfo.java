package com.abhijithsai.shoppingcart.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class CustomerInfo {
	private String name;
	private String address;
	private String email;
	private String phone;

	private boolean valid;
	
}