package com.abhijithsai.shoppingcart.DaoImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import com.abhijithsai.shoppingcart.Dao.AccountDao;
import com.abhijithsai.shoppingcart.entity.Account;

@Transactional
public class AccountDaoImpl implements AccountDao {
	private SessionFactory sessionFactory;
	@Override
	public Account findAccount(String userName) {
		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Account.class);
		crit.add(Restrictions.eq("userName", userName));
		return (Account)crit.uniqueResult();
	}
}
