package com.abhijithsai.shoppingcart.DaoImpl;

import java.util.List;
import java.util.UUID;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.abhijithsai.shoppingcart.Dao.OrderDao;
import com.abhijithsai.shoppingcart.Dao.ProductDao;
import com.abhijithsai.shoppingcart.entity.Order;
import com.abhijithsai.shoppingcart.entity.OrderDetails;
import com.abhijithsai.shoppingcart.entity.Product;
import com.abhijithsai.shoppingcart.model.CartInfo;
import com.abhijithsai.shoppingcart.model.CartLineInfo;
import com.abhijithsai.shoppingcart.model.CustomerInfo;
import com.abhijithsai.shoppingcart.model.OrderDetailInfo;
import com.abhijithsai.shoppingcart.model.OrderInfo;
import com.abhijithsai.shoppingcart.model.PaginationResult;

public class OrderDaoImpl implements OrderDao {
	private SessionFactory sessionFactory;

	@Autowired
	private ProductDao productDao;

	public int getMaxOrderNum() {
		String sql = "Select max(o.orderNum) from" + Order.class.getName() + 'o';
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(sql);
		Integer value = (Integer) query.uniqueResult();
		if (value == null) {
			return 0;
		}
		return value;
	}

	@Override
	public void saveOrder(CartInfo cartInfo) {
		Session session = sessionFactory.getCurrentSession();

		int orderNum = this.getMaxOrderNum() + 1;
		Order order = new Order();

		order.setId(UUID.randomUUID().toString());
		order.setOrderNum(orderNum);
		order.setAmount(cartInfo.getAmountTotal());

		CustomerInfo customerInfo = cartInfo.getCustomerInfo();
		order.setCustomerName(customerInfo.getName());
		order.setCustomerPhone(customerInfo.getPhone());
		order.setCustomerEmail(customerInfo.getEmail());
		order.setCustomerAddress(customerInfo.getAddress());

		session.persist(order);

		List<CartLineInfo> lines = cartInfo.getCartLines();

		for (CartLineInfo line : lines) {
			OrderDetails detail = new OrderDetails();
			detail.setId(UUID.randomUUID().toString());
			detail.setOrder(order);
			detail.setPrice(line.getProductInfo().getPrice());
			detail.setAmount(line.getAmount());
			detail.setQuantity(line.getQuantity());
			String code = line.getProductInfo().getCode();
			Product product = this.productDao.findProduct(code);
			detail.setProduct(product);

			session.persist(detail);
		}
		cartInfo.setOrderNum(orderNum);
	}

	public Order findOrderInfo(String orderId) {
		Session session = sessionFactory.getCurrentSession();
		Criteria crit = session.createCriteria(Order.class);
		crit.add(Restrictions.eq("id", orderId));
		return (Order) crit.uniqueResult();
	}

	@Override
	public OrderInfo getOrderInfo(String orderId) {
		Order order = this.findOrderInfo(orderId);
		if (order == null) {
			return null;
		}
		return new OrderInfo(order.getId(), order.getOrderDate(), order.getOrderNum(), order.getAmount(),
				order.getCustomerName(), order.getCustomerAddress(), order.getCustomerEmail(),
				order.getCustomerPhone());
	}

	@Override
	public List<OrderDetailInfo> listOrderInfo(String orderId) {
		String sql = "Select new" + OrderDetailInfo.class.getName()
				+ "(d.id, d.product.code, d.product.name , d.quanity,d.price,d.amount)" + "from"
				+ OrderDetailInfo.class.getName() + "d" + " where d.order.id = :orderId";
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(sql);
		query.setParameter("orderId", orderId);

		return null;
	}

	@Override
	public PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage) {
		String sql = "Select new " + OrderInfo.class.getName()//
				+ "(ord.id, ord.orderDate, ord.orderNum, ord.amount, "
				+ " ord.customerName, ord.customerAddress, ord.customerEmail, ord.customerPhone) " + " from "
				+ Order.class.getName() + " ord "//
				+ " order by ord.orderNum desc";
		Session session = this.sessionFactory.getCurrentSession();

		Query query = session.createQuery(sql);

		return new PaginationResult<OrderInfo>(query, page, maxResult, maxNavigationPage);
	}

}
