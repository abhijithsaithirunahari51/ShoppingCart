package com.abhijithsai.shoppingcart.Dao;

import com.abhijithsai.shoppingcart.entity.Account;

public interface AccountDao {
	public Account findAccount(String userName);
}
