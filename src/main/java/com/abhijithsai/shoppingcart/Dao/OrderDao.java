package com.abhijithsai.shoppingcart.Dao;

import java.util.List;

import com.abhijithsai.shoppingcart.model.CartInfo;
import com.abhijithsai.shoppingcart.model.OrderDetailInfo;
import com.abhijithsai.shoppingcart.model.OrderInfo;
import com.abhijithsai.shoppingcart.model.PaginationResult;

public interface OrderDao {
	public void saveOrder(CartInfo cartInfo);
	public OrderInfo getOrderInfo(String orderId);
	public List<OrderDetailInfo> listOrderInfo(String orderId);
	public PaginationResult<OrderInfo> listOrderInfo(int page, int maxResult, int maxNavigationPage);
}
