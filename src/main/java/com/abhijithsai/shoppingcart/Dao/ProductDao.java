package com.abhijithsai.shoppingcart.Dao;

import com.abhijithsai.shoppingcart.entity.Product;
import com.abhijithsai.shoppingcart.model.PaginationResult;
import com.abhijithsai.shoppingcart.model.ProductInfo;

public interface ProductDao {
	public Product findProduct(String code);
	public ProductInfo findProductInfo(String code);
	
	public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage);
	public PaginationResult<ProductInfo> queryProducts(int page, int maxResult, int maxNavigationPage, String likeName);
	public void save(ProductInfo productInfo);
}
