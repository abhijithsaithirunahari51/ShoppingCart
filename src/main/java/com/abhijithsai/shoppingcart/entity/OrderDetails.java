package com.abhijithsai.shoppingcart.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="order_details")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetails {
	@Id
	@Column(name="ID",length=50,nullable=false)
	private String id;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ORDER_ID",nullable=false,foreignKey=@ForeignKey(name="ORDER_DETAIL_ORD_FK"))
	private Order order;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="ORDER_ID",nullable=false,foreignKey=@ForeignKey(name="ORDER_DETAIL_PROD_FK"))
	private Product product;
	
	@Column(name="quantity",nullable=false)
	private int quantity;
	
	@Column(name="price", nullable=false)
	private double price;
	
	@Column(name="amount",nullable=false)
	private double amount;
	
}
