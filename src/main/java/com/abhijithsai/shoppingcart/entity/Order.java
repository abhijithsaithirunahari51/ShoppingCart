package com.abhijithsai.shoppingcart.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="orders")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order implements Serializable{
	private static final long serialVersionUID = -2576670215015463100L;
	
	@Id
	@Column(name="ID",length=50)
	private String id;
	
	@Column(name="Order_Date",nullable=false)
	private Date orderDate;
	
	@Column(name="Order_Num",nullable=false)
	private int orderNum;
	
	@Column(name="Amount",nullable=false)
	private double amount;
	
	@Column(name="Customer-Name",length = 255, nullable=false)
	private String customerName;
	
	@Column(name="Customer-Email",length = 128, nullable=false)
	private String customerEmail;
	
	@Column(name="Customer-Phone",length = 10, nullable=false)
	private String customerPhone;
	
	@Column(name="Customer-Address",length = 255, nullable=false)
	private String customerAddress;
	
	
}
